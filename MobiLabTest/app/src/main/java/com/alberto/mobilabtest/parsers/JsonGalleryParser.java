package com.alberto.mobilabtest.parsers;

import com.alberto.mobilabtest.model.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * JsonGalleryParser.java
 * Purpose: Parse data from a Json String to an Array of Image objects
 *
 * @author Alberto Robles
 * @version 1.0 12/03/15
 */
public class JsonGalleryParser {

    private ArrayList<Image> gallery;
    private Image image;

    public ArrayList<Image> parse(String connectionResponse){

        if(connectionResponse!=null){

            try {
                JSONObject rootObject = new JSONObject(connectionResponse);

                if(!rootObject.isNull("data")){
                    JSONArray dataArray = rootObject.getJSONArray("data");
                    gallery= new ArrayList<>();

                    for(int i=0; i < dataArray.length();i++){
                        image=new Image();
                        JSONObject imageObject = dataArray.getJSONObject(i);

                        if(!imageObject.isNull("id")){
                            image.setId(imageObject.getString("id"));
                        }
                        if(!imageObject.isNull("title")){
                            image.setTitle(imageObject.getString("title"));
                        }
                        if(!imageObject.isNull("description")){
                            image.setDescription(imageObject.getString("description"));
                        }
                        if(!imageObject.isNull("datetime")){
                            image.setDatetime(imageObject.getLong("datetime"));
                        }
                        if(!imageObject.isNull("type")){
                            image.setType(imageObject.getString("type"));
                        }
                        if(!imageObject.isNull("animated")){
                            image.setAnimated(imageObject.getBoolean("animated"));
                        }
                        if(!imageObject.isNull("width")){
                            image.setWidth(imageObject.getInt("width"));
                        }
                        if(!imageObject.isNull("height")){
                            image.setHeight(imageObject.getInt("height"));
                        }
                        if(!imageObject.isNull("size")){
                            image.setSize(imageObject.getInt("size"));
                        }
                        if(!imageObject.isNull("views")){
                            image.setViews(imageObject.getInt("views"));
                        }
                        if(!imageObject.isNull("comment_count")){
                            image.setCommentCount(imageObject.getInt("comment_count"));
                        }
                        if(!imageObject.isNull("bandwidth")){
                            image.setBandwidth(imageObject.getInt("bandwidth"));
                        }
                        if(!imageObject.isNull("section")){
                            image.setSection(imageObject.getString("section"));
                        }
                        if(!imageObject.isNull("account_url")){
                            image.setAccountUrl(imageObject.getString("account_url"));
                        }
                        if(!imageObject.isNull("account_id")){
                            image.setAccountId(imageObject.getInt("account_id"));
                        }
                        if(!imageObject.isNull("ups")){
                            image.setUps(imageObject.getInt("ups"));
                        }
                        if(!imageObject.isNull("downs")){
                            image.setDowns(imageObject.getInt("downs"));
                        }
                        if(!imageObject.isNull("score")){
                            image.setScore(imageObject.getInt("score"));
                        }
                        if(!imageObject.isNull("is_album")){
                            image.setIsAlbum(imageObject.getBoolean("is_album"));
                        }

                        gallery.add(image);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return gallery;
    }

}
