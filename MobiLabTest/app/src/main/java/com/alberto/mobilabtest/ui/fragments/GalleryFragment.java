package com.alberto.mobilabtest.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

import com.alberto.mobilabtest.R;
import com.alberto.mobilabtest.connections.ConnectionHandler;
import com.alberto.mobilabtest.model.Image;
import com.alberto.mobilabtest.model.Constants;
import com.alberto.mobilabtest.parsers.JsonGalleryParser;
import com.alberto.mobilabtest.ui.activities.MainActivity;
import com.alberto.mobilabtest.ui.adapters.GalleryAdapter;
import com.alberto.mobilabtest.ui.adapters.StaggeredGridAdapter;
import com.alberto.mobilabtest.utils.Utils;
import com.etsy.android.grid.StaggeredGridView;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;

/**
 * Galleryfragment.java
 * Purpose: Consumes the gallery feed, displays the information, adn manages view layout changes
 * @author Alberto Robles
 * @version 1.0 12/03/15
 */
public class GalleryFragment extends Fragment implements AbsListView.OnScrollListener {

    private static final String GALLERY_TYPE = "type";

    private enum DialogType {SORT,WINDOW}
    private enum ViewType {GRID,LIST,STAGGERED}

    private ViewType currentType =ViewType.GRID;
    private ConnectionHandler connectionHandler;
    private ArrayList<Image> gallery;

    private ProgressWheel progressBar;
    private GridView gridViewGallery;
    private ListView listViewGallery;
    private StaggeredGridView staggeredGridView;
    private boolean showViral=true;
    private int galleryType;
    private String sortOption=Constants.SortOptions.getSortOptions(0);
    private String windowOptions=Constants.WindowOptions.getWindowOptions(0);

    private int mLastFirstVisibleItem = 0;

    public static GalleryFragment newInstance(int galleryType){
        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.setRetainInstance(true);
        Bundle bundle = new Bundle();
        bundle.putInt(GALLERY_TYPE,galleryType);
        galleryFragment.setArguments(bundle);

        return galleryFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments()!=null){
           galleryType= getArguments().getInt(GALLERY_TYPE,0);
        }

        progressBar = (ProgressWheel) view.findViewById(R.id.progress_bar);
        gridViewGallery = (GridView) view.findViewById(R.id.gridView_gallery);
        listViewGallery=(ListView)view.findViewById(R.id.listView_gallery);
        staggeredGridView= (StaggeredGridView) view.findViewById(R.id.staggered_gallery);

        if(gallery==null){
            getGallery();
        }else{
            setLayout(currentType);
        }
    }

    private void getGallery() {
        connectionHandler = new ConnectionHandler(getActivity()) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                gridViewGallery.setVisibility(View.GONE);
                listViewGallery.setVisibility(View.GONE);
                staggeredGridView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                progressBar.setVisibility(View.GONE);

                if (connectionResponse != null) {
                    JsonGalleryParser parser = new JsonGalleryParser();
                    gallery = parser.parse(connectionResponse);
                    setLayout(currentType);
                }
            }
        };

        connectionHandler.execute(Constants.UrlManager.getGalleryEndpoint(showViral,galleryType,sortOption,windowOptions));
    }

    /**Sets layout according the the selected view type**/
    private void setLayout(ViewType viewType){

        AdapterView.OnItemClickListener onItemClickListener=new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ((MainActivity)getActivity()).replaceFragment(ImageDetailFragment.newInstance(gallery.get(position).getId(),
                        gallery.get(position).getTitle(),
                        gallery.get(position).getDescription(),
                        gallery.get(position).getUps(),
                        gallery.get(position).getDowns(),
                        gallery.get(position).getScore()),
                        "detail", true, true);
            }
        };

        if(viewType.equals(ViewType.GRID)){
            gridViewGallery.setVisibility(View.VISIBLE);
            listViewGallery.setVisibility(View.GONE);
            staggeredGridView.setVisibility(View.GONE);

            gridViewGallery.setAdapter(new GalleryAdapter(getActivity(), gallery));
            gridViewGallery.setOnItemClickListener(onItemClickListener);
            gridViewGallery.setOnScrollListener(this);

            currentType=ViewType.GRID;

        }else if(viewType.equals(ViewType.LIST)){
            gridViewGallery.setVisibility(View.GONE);
            listViewGallery.setVisibility(View.VISIBLE);
            staggeredGridView.setVisibility(View.GONE);

            listViewGallery.setAdapter(new GalleryAdapter(getActivity(), gallery));
            listViewGallery.setOnItemClickListener(onItemClickListener);
            listViewGallery.setOnScrollListener(this);

            currentType=ViewType.LIST;

        }else{
            gridViewGallery.setVisibility(View.GONE);
            listViewGallery.setVisibility(View.GONE);
            staggeredGridView.setVisibility(View.VISIBLE);

            staggeredGridView.setAdapter(new StaggeredGridAdapter(getActivity(),gallery));
            staggeredGridView.setOnItemClickListener(onItemClickListener);
            staggeredGridView.setOnScrollListener(this);

            currentType=ViewType.STAGGERED;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utils.cancelTask(connectionHandler);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        //We only show the window option in the Top section, as specified in the Imgur API
        if(!Constants.GalleryTypes.getGalleryType(galleryType).equals(Constants.GalleryTypes.TOP)){
            menu.findItem(R.id.menu_window).setVisible(false);
        }
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_viral:
                showViral = showViral ? false : true;
                getGallery();
            break;

            case R.id.menu_sort:
                buildDialog(DialogType.SORT);
                break;

            case R.id.menu_window:
                buildDialog(DialogType.WINDOW);
                break;

            case R.id.menu_view_list:
                setLayout(ViewType.LIST);
                break;

            case R.id.menu_view_grid:
                setLayout(ViewType.GRID);
                break;

            case R.id.menu_view_staggered:
                setLayout(ViewType.STAGGERED);
                break;
        }
        return true;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_viral).setChecked(showViral);
    }

    /**Builds dialog for yhe window and sort options**/
    private void buildDialog(final DialogType dialogType){
        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                getActivity());
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_selectable_list_item);

        if(dialogType.equals(DialogType.SORT)) {
            arrayAdapter.add(Constants.SortOptions.getSortOptions(0));
            arrayAdapter.add(Constants.SortOptions.getSortOptions(1));
            arrayAdapter.add(Constants.SortOptions.getSortOptions(2));
            arrayAdapter.add(Constants.SortOptions.getSortOptions(3));
        }else{
            arrayAdapter.add(Constants.WindowOptions.getWindowOptions(0));
            arrayAdapter.add(Constants.WindowOptions.getWindowOptions(1));
            arrayAdapter.add(Constants.WindowOptions.getWindowOptions(2));
            arrayAdapter.add(Constants.WindowOptions.getWindowOptions(3));
        }
        builderSingle.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        if(dialogType.equals(DialogType.SORT)) {
                            sortOption=strName;
                        }else{
                            windowOptions=strName;
                        }
                        getGallery();
                        dialog.dismiss();
                    }
                });
        builderSingle.create();
        builderSingle.show();
    }

    /**Listeners for handling the action bar hiding/showing**/
    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {  }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if (view.getId() == view.getId()) {
            final int currentFirstVisibleItem = view.getFirstVisiblePosition();

            if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                ((ActionBarActivity)getActivity()).getSupportActionBar().hide();
            } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                ((ActionBarActivity)getActivity()).getSupportActionBar().show();
            }

            mLastFirstVisibleItem = currentFirstVisibleItem;
        }
    }

}
