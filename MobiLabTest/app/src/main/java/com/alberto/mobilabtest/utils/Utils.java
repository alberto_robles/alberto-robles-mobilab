package com.alberto.mobilabtest.utils;

import android.os.AsyncTask;

/**
 * Utils.java
 * @author Alberto Robles
 * @version 1.0 12/03/15
 * */
public class Utils {

    /**Cancels any Running async task**/
    public static void cancelTask(AsyncTask task){
        if(task!=null)
        {
            if(task.getStatus()== AsyncTask.Status.RUNNING){
                task.cancel(true);
            }
        }
    }

}
