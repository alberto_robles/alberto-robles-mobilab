package com.alberto.mobilabtest.ui.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alberto.mobilabtest.R;
import com.alberto.mobilabtest.model.Constants;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.pnikosis.materialishprogress.ProgressWheel;

/**
 * ImageDetailFragment.java
 * Purpose: Loads the image and displays the picture information
 * @author Alberto Robles
 * @version 1.0 12/03/15
 * */
public class ImageDetailFragment extends Fragment{

    private static final String SCORE = "score";
    private static final String DOWNVOTES = "downvotes";
    private static final String UPVOTES = "upvotes";
    private static final String DESCRIPTION = "description";
    private static final String TITLE = "title";
    private static final String ID = "id";

    private TextView txtTitle,txtDescription,txtUpvotes,txtDownvotes,txtScore;
    private ImageView imgBig;
    private String id, title, description;
    private int upvotes, downvotes, score;
    private ProgressWheel progressBar;

    public static ImageDetailFragment newInstance(String id, String title, String description,int upvotes,int downvotes,int score) {

        ImageDetailFragment imageDetailFragment = new ImageDetailFragment();
        imageDetailFragment.setRetainInstance(true);

        Bundle bundle = new Bundle();
        bundle.putString(ID,id);
        bundle.putString(TITLE,title);
        bundle.putString(DESCRIPTION,description);
        bundle.putInt(UPVOTES, upvotes);
        bundle.putInt(DOWNVOTES, downvotes);
        bundle.putInt(SCORE, score);

        imageDetailFragment.setArguments(bundle);
        return imageDetailFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = (ProgressWheel) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        txtTitle=(TextView)view.findViewById(R.id.txt_title);
        txtDescription=(TextView)view.findViewById(R.id.txt_description);
        txtUpvotes=(TextView)view.findViewById(R.id.txt_upvotes);
        txtDownvotes=(TextView)view.findViewById(R.id.txt_downvotes);
        txtScore=(TextView)view.findViewById(R.id.txt_score);
        imgBig=(ImageView)view.findViewById(R.id.img_detail);

        if(getArguments()!=null){
            id=getArguments().getString(ID,"");
            title=getArguments().getString(TITLE,"");
            description=getArguments().getString(DESCRIPTION,"");
            upvotes=getArguments().getInt(UPVOTES, 0);
            downvotes=getArguments().getInt(DOWNVOTES, 0);
            score=getArguments().getInt(SCORE, 0);
        }

        txtTitle.setText(title);
        txtDescription.setText(description);
        txtUpvotes.setText(txtUpvotes.getText()+": "+upvotes);
        txtDownvotes.setText(txtDownvotes.getText()+": "+downvotes);
        txtScore.setText(txtScore.getText()+": "+score);

        ImageLoader.getInstance().loadImage(Constants.UrlManager.getImageUrl(id, Constants.Resolutions.LARGE), new SimpleImageLoadingListener(){
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                progressBar.setVisibility(View.GONE);
                imgBig.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                super.onLoadingFailed(imageUri, view, failReason);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        ((ActionBarActivity)getActivity()).getSupportActionBar().show();
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().getSupportFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
