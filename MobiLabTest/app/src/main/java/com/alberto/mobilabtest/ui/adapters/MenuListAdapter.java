package com.alberto.mobilabtest.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alberto.mobilabtest.R;
import com.alberto.mobilabtest.model.Constants;

/**
 * MenuListAdapter.java
 * Purpose: Adapter for the drawer's listview
 * @author Alberto Robles
 * @version 1.0 12/03/15
 */

public class MenuListAdapter extends BaseExpandableListAdapter{

    private LayoutInflater inflater;

    public MenuListAdapter(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getGroupCount() {
        return Constants.Menu.menuItems.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(groupPosition==0) {
            return Constants.Menu.galleryMenuItems.length;
        }else {
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return Constants.Menu.menuItems[groupPosition];
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if(groupPosition==0) {
            return Constants.Menu.galleryMenuItems[childPosition];
        }else {
            return null;
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_menu_list, null);
            holder = new ViewHolder();
            holder.txtTitle=(TextView) convertView.findViewById(R.id.txt_title);
            holder.indicator=(ImageView) convertView.findViewById(R.id.indicator);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtTitle.setText(Constants.Menu.menuItems[groupPosition]);

        //Hide indicator for empty groups
        if (groupPosition == 1 ) {
            holder.indicator.setVisibility(View.INVISIBLE);
        }else{
            holder.indicator.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_menu_list, null);
            holder = new ViewHolder();
            holder.txtTitle=(TextView) convertView.findViewById(R.id.txt_title);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtTitle.setText(Constants.Menu.galleryMenuItems[childPosition]);

        return convertView;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ViewHolder {
        TextView txtTitle;
        ImageView indicator;
    }
}
