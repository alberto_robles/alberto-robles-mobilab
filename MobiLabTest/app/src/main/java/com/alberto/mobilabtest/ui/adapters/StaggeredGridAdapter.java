package com.alberto.mobilabtest.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.alberto.mobilabtest.R;
import com.alberto.mobilabtest.model.Image;
import com.alberto.mobilabtest.model.Constants;
import com.etsy.android.grid.util.DynamicHeightImageView;
import com.etsy.android.grid.util.DynamicHeightTextView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * StaggeredGridAdapter.java
 * Purpose: Adapter for the StaggeredGridView
 * @author Alberto Robles
 * @version 1.0 13/03/15
 */

public class StaggeredGridAdapter extends BaseAdapter {

    private static final String TAG = "SampleAdapter";

    private List<Image> gallery;
    private LayoutInflater inflater;

    private final Random mRandom;
    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public StaggeredGridAdapter(Context context, ArrayList<Image>  gallery) {
        this.gallery=gallery;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mRandom = new Random();
    }

    @Override
    public int getCount() {
        return gallery.size();
    }

    @Override
    public Object getItem(int position) {
        return gallery.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_gallery_staggered,parent, false);
            holder = new ViewHolder();
            holder.imgView = (DynamicHeightImageView) convertView.findViewById(R.id.img_gallery);
            holder. mTextDescription=(DynamicHeightTextView)convertView.findViewById(R.id.txt_description);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //Random heights for the staggered effect
        double positionHeight = getPositionRatio(position);
        holder.imgView.setHeightRatio(positionHeight);

        ImageLoader.getInstance().displayImage(Constants.UrlManager.getImageUrl(gallery.get(position).getId(), Constants.Resolutions.SQUARE), holder.imgView);
        holder.mTextDescription.setText(gallery.get(position).getDescription());

        return convertView;
    }


    static class ViewHolder {
        DynamicHeightImageView imgView;
        DynamicHeightTextView mTextDescription;
    }

    /**Get random positions for the staggered effect**/
    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
            Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0;
    }
}
