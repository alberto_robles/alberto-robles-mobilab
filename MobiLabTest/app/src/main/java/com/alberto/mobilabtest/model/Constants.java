package com.alberto.mobilabtest.model;

/**
 * Constants.java
 * Purpose: Contains all the app's constants and manages the building of the app's urls
 *
 * @author Alberto Robles
 * @version 1.0 12/03/15
 */
public class Constants {

    public static class Menu{
        public static String[] menuItems = {"Gallery","About"};
        public static String[] galleryMenuItems={"Hot","Top","User"};
    }

    public static class HeadersManager{

        public static final String HEADER_NAME_CLIENT_ID="Authorization";
        private static final String HEADER_PARAM_CLIENT_ID= "Client-ID";
        private static final String CLIENT_ID ="e4e765f1841f473";

        public static String getClientIdHeader(){
            return HEADER_PARAM_CLIENT_ID+" "+CLIENT_ID;
        }
    }

    public static class UrlManager {

        private static final String HOST="https://api.imgur.com/3";
        private static final String ENDPOINT_GALLERY="/gallery/";
        private static final String ENDPOINT_VIRAL="/?showViral=";
        private static final String URL_IMAGES="http://i.imgur.com/";

        public static String getGalleryEndpoint(boolean showViral,int galleryType,String sortOption, String windowOption){
            return HOST+ENDPOINT_GALLERY
                    +GalleryTypes.getGalleryType(galleryType)+"/"
                    +sortOption+"/"
                    +windowOption
                    +ENDPOINT_VIRAL+showViral;
        }


        public static String getImageUrl(String id,String resolution){
            return URL_IMAGES+id+resolution+".jpg";
        }
    }

    public static class Resolutions{
        public static final String HUGE="h";
        public static final String LARGE="l";
        public static final String MEDIUM="m";
        public static final String SMALL="m";
        public static final String SQUARE="b";
    }

    public static class GalleryTypes{
        public static final String HOT="hot";
        public static final String TOP="top";
        public static final String USER="user";

        public static String getGalleryType(int galleryType){
            switch (galleryType){
                case 0:
                    return HOT;
                case 1:
                    return TOP;
                case 2:
                    return USER;
                default:
                    return HOT;
            }
        }
    }

    public static class SortOptions{
        public static final String VIRAL="viral";
        public static final String TOP="top";
        public static final String TIME="time";
        public static final String RISING="rising";

        public static String getSortOptions(int sortOption){
            switch (sortOption){
                case 0:
                    return VIRAL;
                case 1:
                    return TOP;
                case 2:
                    return TIME;
                case 3:
                    return RISING;
                default:
                    return VIRAL;
            }
        }
    }

    public static class WindowOptions{
        public static final String DAY="day";
        public static final String WEEK="week";
        public static final String MONTH="month";
        public static final String YEAR="year";

        public static String getWindowOptions(int windowOption){
            switch (windowOption){
                case 0:
                    return DAY;
                case 1:
                    return WEEK;
                case 2:
                    return MONTH;
                case 3:
                    return YEAR;
                default:
                    return DAY;
            }
        }
    }

}
