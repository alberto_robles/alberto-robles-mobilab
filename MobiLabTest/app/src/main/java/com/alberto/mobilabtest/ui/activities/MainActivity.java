package com.alberto.mobilabtest.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.alberto.mobilabtest.R;
import com.alberto.mobilabtest.ui.adapters.MenuListAdapter;
import com.alberto.mobilabtest.ui.fragments.AboutFragment;
import com.alberto.mobilabtest.ui.fragments.GalleryFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * MainActivity.java
 *
 * @author Alberto Robles
 * @version 1.0 12/03/15
 */
public class MainActivity extends ActionBarActivity {

    private static final String GALLERY = "gallery";

    private DrawerLayout mDrawerLayout;
    private ExpandableListView mListMenu;
    private ActionBarDrawerToggle mDrawerToggle;
    private GalleryFragment galleryFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize image loading library
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
        .cacheOnDisk(true)
        .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
        .defaultDisplayImageOptions(defaultOptions)
        .build();

        ImageLoader.getInstance().init(config);

        //Init UI components
        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        mListMenu=(ExpandableListView)findViewById(R.id.drawer_list);

        //Init Action Bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Init Drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        //Init Menu
        mListMenu.setAdapter(new MenuListAdapter(this));
        mListMenu.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if(groupPosition==1){
                    replaceFragment(new AboutFragment(),"aboutFragmnent",true,false);
                    mDrawerLayout.closeDrawers();
                    return true;
                }else{
                    return false;
                }
            }
        });

        mListMenu.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                mDrawerLayout.closeDrawers();
                replaceFragment(GalleryFragment.newInstance(childPosition),"listMenu",true,false);
                return false;
            }
        });

        //Present gallery fragment and handle screen rotation
        if (savedInstanceState != null) {
            galleryFragment = (GalleryFragment) getSupportFragmentManager().findFragmentByTag(GALLERY);
        } else {
            galleryFragment = new GalleryFragment();
            galleryFragment.setRetainInstance(true);
            replaceFragment(galleryFragment,GALLERY,false,false);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**Replaces fragments in the main app's container**/
    public void replaceFragment(Fragment fragment, String tag,boolean isAnimated,boolean addToBackStack ){

       FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
       if(isAnimated){
           fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left,android.R.anim.slide_out_right,android.R.anim.fade_in,android.R.anim.fade_out);
       }
       if(addToBackStack){
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.replace(R.id.main_container, fragment, tag);
        fragmentTransaction.commit();

    }

}
