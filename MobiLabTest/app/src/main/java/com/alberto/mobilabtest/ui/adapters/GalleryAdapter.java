package com.alberto.mobilabtest.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alberto.mobilabtest.R;
import com.alberto.mobilabtest.model.Image;
import com.alberto.mobilabtest.model.Constants;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * GalleryAdapter.java
 * Purpose: Adapter for the gallery section,serving the gridview and listview widgets
 * @author Alberto Robles
 * @version 1.0 12/03/15
 */

public class GalleryAdapter extends BaseAdapter {

    private List<Image> gallery;
    private LayoutInflater inflater;

    public GalleryAdapter(Context context, List<Image> gallery) {
        this.gallery=gallery;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return gallery.size();
    }

    @Override
    public Object getItem(int position) {
        return gallery.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null){
            convertView = inflater.inflate(R.layout.item_gallery, null);
            holder = new ViewHolder();
            holder.mImageView=(ImageView) convertView.findViewById(R.id.img_gallery);
            holder. mTextDescription=(TextView)convertView.findViewById(R.id.txt_description);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextDescription.setText(gallery.get(position).getDescription());
        ImageLoader.getInstance().displayImage(Constants.UrlManager.getImageUrl(gallery.get(position).getId(),Constants.Resolutions.SQUARE), holder.mImageView);

        return convertView;
    }

    private class ViewHolder{
        ImageView mImageView;
        TextView mTextDescription;
    }

}
