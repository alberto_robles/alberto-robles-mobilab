package com.alberto.mobilabtest.connections;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.alberto.mobilabtest.model.Constants;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * ConnectionHandler.java
 * Purpose: An AsyncTask to handle http connections
 * @author Alberto Robles
 * @version 1.0 12/03/15
 */
public class ConnectionHandler  extends AsyncTask <String, Object, Object> {

    private final String DEBUG_TAG ="ConnectionHandler";

    private Context mContext;
    private boolean showLog = true;

    private String url;
    private int responseCode;

    public String connectionResponse;
    private URLConnection connection;
    private List<NameValuePair> headerParams;

    public ConnectionHandler(Context context) {
        this.mContext = context;
    }

    @Override
    protected Object doInBackground(String... params) {

        if (params != null) {
            url = params[0];
            if (hasInternetConnection(mContext)) {
                connectionResponse = getConnectionResponse();
            }
        }

        wsLog("=================================");
        if (connectionResponse != null) {
            wsLog("RESPONSE: " + connectionResponse);
        } else {
            wsLog("THERE'S NO RESPONSE");
        }
        wsLog("=================================");

        return connectionResponse;
    }

    /**verifies if the app is connected to any network**/
    public static boolean hasInternetConnection(Context context) {
        if (context != null) {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (manager != null) {
                NetworkInfo networkInfo = manager.getActiveNetworkInfo();
                if (networkInfo != null) {
                    return networkInfo.isConnected();
                }
            }
        }

        return false;
    }

    /**Makes the request to the server, validating if its a http or a https connection**/
    private String getConnectionResponse() {

        String response = null;
        try {

            wsLog("=================================");
            wsLog("REQUEST TO: " + url);
            wsLog("=================================");

            URL urlRequest = new URL(url);

            connection = urlRequest.openConnection();

            wsLog("=================================");
            wsLog("HEADERS");

            connection.setRequestProperty(Constants.HeadersManager.HEADER_NAME_CLIENT_ID, Constants.HeadersManager.getClientIdHeader());
            wsLog(Constants.HeadersManager.HEADER_NAME_CLIENT_ID + " -- " + Constants.HeadersManager.getClientIdHeader());

            wsLog("=================================");

            if (connection instanceof HttpsURLConnection) {

                wsLog("=================================");
                wsLog("HTTPS CHANNEL REQUEST");
                wsLog("=================================");

                HttpsURLConnection secureConnection = (HttpsURLConnection) connection;

                secureConnection.setUseCaches(false);
                SSLContext sslcontext = SSLContext.getInstance("TLS");
                sslcontext.init(null, new TrustManager[]{new CustomX509TrustManager(null)}, null);

                secureConnection.setSSLSocketFactory(sslcontext.getSocketFactory());

                connection = secureConnection;

            } else {
                wsLog("=================================");
                wsLog("NOT HTTPS CHANNEL REQUEST");
                wsLog("=================================");
            }

            wsLog("=================================");
            wsLog("GET REQUEST");
            wsLog("=================================");

            try {
                ((HttpURLConnection) connection).setRequestMethod("GET");
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            }


            try {
                this.responseCode = (((HttpURLConnection) connection).getResponseCode());
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            wsLog("=================================");
            wsLog("RESPONSE CODE: " + responseCode);
            wsLog("=================================");

            InputStream inputStream = null;
            if (responseCode >= HttpStatus.SC_BAD_REQUEST) {
                inputStream = ((HttpURLConnection) connection).getErrorStream();
            } else{
                try {
                    inputStream = connection.getInputStream();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            if(inputStream!=null) {
                response = readInputStreamAsString(inputStream);
                inputStream.close();
            }


        } catch (Exception e) {

            wsLog("=================================");
            wsLog("getConnectionResponse() "+e.toString());
            wsLog("=================================");

        } finally {
            ((HttpURLConnection) connection).disconnect();
        }

        return response;
    }

    private String readInputStreamAsString(InputStream in) throws IOException {

        BufferedInputStream bis = new BufferedInputStream(in);
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int result = bis.read();
        while(result != -1) {
            byte b = (byte)result;
            buf.write(b);
            result = bis.read();
        }
        String response=buf.toString();
        buf.close();
        bis.close();
        in.close();
        return response;
    }


    /**https trust manager**/
    private class CustomX509TrustManager implements X509TrustManager {

        private X509TrustManager standardTrustManager = null;

        public CustomX509TrustManager(KeyStore keystore)
                throws NoSuchAlgorithmException, KeyStoreException {
            super();
            TrustManagerFactory factory = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());

            factory.init(keystore);
            TrustManager[] trustmanagers = factory.getTrustManagers();

            if (trustmanagers.length == 0) {
                throw new NoSuchAlgorithmException("NO TRUST MANAGER FOUND");
            }
            this.standardTrustManager = (X509TrustManager) trustmanagers[0];
        }

        public void checkClientTrusted(X509Certificate[] certificates,
                                       String authType) throws CertificateException {
            standardTrustManager.checkClientTrusted(certificates, authType);
        }

        public void checkServerTrusted(X509Certificate[] certificates,
                                       String authType) throws CertificateException {
            if ((certificates != null) && (certificates.length == 1)) {
                certificates[0].checkValidity();
            } else {
                standardTrustManager.checkServerTrusted(certificates, authType);
            }
        }

        public X509Certificate[] getAcceptedIssuers() {
            return this.standardTrustManager.getAcceptedIssuers();
        }
    }


    /**Enables disable ConnectionHandler Log**/
    public void enableLog() {
        showLog = true;
    }

    private void wsLog(String msg) {
        if (showLog) {
            Log.i(DEBUG_TAG, msg);
        }
    }

}